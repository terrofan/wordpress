package sanity.user;

import com.telran.api.ApiClient;
import com.telran.app.user.User;
import com.telran.app.user.UserApiHelper;
import com.telran.app.user.UserDBHelper;
import com.telran.common.RandomDataGenerator;
import com.telran.report.TestReport;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class CreateUserWithApiTest {
    @Test(groups = "sanity")
    public void createUserTest() throws IOException, InterruptedException {
        TestReport.info("====> Generating random user");
        User user = RandomDataGenerator.generateFakeUser();
        TestReport.info("New user info: " + user.toString());
        TestReport.info("=====> Sending request to create new User");
        ApiClient.sendUserPostRequest(user.newUserRequestAsString());
        TestReport.info("=====> Verifying data in response");
        Assert.assertTrue(ApiClient.verifyResponseCode(201));
        Assert.assertTrue(ApiClient.verifyFieldInResponse("username", user.getUsername()));
        Assert.assertTrue(UserApiHelper.findUserInResponse("name", user));
        //Check record in DB
        TestReport.info("====> Verifying data in DB");
        User dbUser = UserDBHelper.createUserObjectByUsername(user.getUsername());
        Assert.assertEquals(dbUser, user);
        // Check user in GUI
//        LoginPage.loginToWordpress();
//        DashBoardPage.navigateToUserPage();
//        UserPage.isOnPage();
//        UserPage.findUserInUserTable("user16");
    }

    @Test(groups = "sanity")
    public void createUserWithJinjavaTest() throws IOException {
        TestReport.info("====> Generating random user");
        User user = RandomDataGenerator.generateFakeUser();
        TestReport.info("New user info: " + user.toString());
        TestReport.info("=====> Sending request to create new User");
        ApiClient.sendUserPostRequest(RandomDataGenerator.generateNewUserRequestFromTemplateFile("user-context.json.j2", user));
        TestReport.info("=====> Verifying data in response");
        Assert.assertTrue(ApiClient.verifyResponseCode(201));
        Assert.assertTrue(ApiClient.verifyFieldInResponse("username", user.getUsername()));
        Assert.assertTrue(UserApiHelper.findUserInResponse("name", user));
        //Check record in DB
        TestReport.info("====> Verifying data in DB");
        User dbUser = UserDBHelper.createUserObjectByUsername(user.getUsername());
        Assert.assertEquals(user, dbUser);
        // Check user in GUI
//        LoginPage.loginToWordpress();
//        DashBoardPage.navigateToUserPage();
//        UserPage.isOnPage();
//        UserPage.findUserInUserTable("user16");
    }


}
