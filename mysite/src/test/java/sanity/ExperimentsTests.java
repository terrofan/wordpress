package sanity;

import com.github.javafaker.Faker;
import com.telran.api.ApiClient;
import com.telran.app.user.User;
import com.telran.app.user.UserApiHelper;
import com.telran.app.user.UserDBHelper;
import com.telran.common.RandomDataGenerator;
import com.telran.gui.DashboardPage;
import com.telran.gui.LoginPage;
import com.telran.report.TestReport;
import com.telran.tests.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;

public class ExperimentsTests extends TestBase {
    @Test(groups = "experiments")
    public void experimentTest(){
        TestReport.info("Test case starting");
        TestReport.debug("...methods calling...");
        TestReport.error("Error occurred");
        TestReport.warn("WARNING");
    }

    @Test
    public void fakeUserTest(){
        Faker faker = new Faker();

        System.out.println(faker.name().firstName());
        System.out.println(faker.name().lastName());
        System.out.println(faker.name().fullName());
        System.out.println(faker.name().username());

        System.out.println(faker.address().city());
        System.out.println(faker.address().country());
        System.out.println(faker.address().fullAddress());
    }

    @Test
    public void findUserByUsername(){
        User user = new User();
        user.setUsername("user");
        boolean res = UserApiHelper.findUserInResponse("name", user);
        Assert.assertTrue(res);
    }

    @Test
    public void createUserObjectFromDB(){
        UserDBHelper.createUserObjectByUsername("user");
    }

    @Test
    public void createUserWithJinJa(){

        User user = RandomDataGenerator.generateFakeUser();

        ApiClient.sendUserPostRequest(RandomDataGenerator.generateNewUserRequestFromTemplateFile("user-context.json.j2", user));
        Assert.assertTrue(UserApiHelper.findUserInResponse("name", user));

    }

    @Test
    public void user_is_on_login_page() {
        init();
        LoginPage loginPage = new LoginPage();
        loginPage.getLoginPage();
    }


    @Test
    public void userLogsInWithUsernameAndPassword() {
        LoginPage loginPage = new LoginPage();
        wd.findElement(loginPage.getUsername()).sendKeys("user");
        wd.findElement(loginPage.getPassword()).sendKeys("password");
        wd.findElement(loginPage.getLoginButton()).click();

    }

    @Test
    public void userLandsOnDashboardPage() {
        DashboardPage dash = new DashboardPage(wd);
        String expectedURL = dash.getDashboardURL();
        String actualURL = wd.getCurrentUrl();
        Assert.assertEquals(expectedURL, actualURL);
    }

    @Test
    public void userSeeAWelcomeMessage() {
        System.out.println("Checking if welcome message is displayed");
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        ExperimentsTests experimentsTests = new ExperimentsTests();
        experimentsTests.user_is_on_login_page();
        experimentsTests.userLogsInWithUsernameAndPassword();
        experimentsTests.userLandsOnDashboardPage();
        experimentsTests.userSeeAWelcomeMessage();
//        String role = "\"author\"";
//        System.out.println(role.replaceAll("\"", ""));
//        User userWithSomeRoles = UserDBHelper.createUserObjectByUsername("user18");
//        User userWithOneRole = UserDBHelper.createUserObjectByUsername("estelle.champlin");
//        User userWithoutRoles = UserDBHelper.createUserObjectByUsername("user5");
//        System.out.println(userWithSomeRoles.toString());
//        System.out.println(userWithOneRole.toString());
//        System.out.println(userWithoutRoles.toString());
//        UserDBHelper.getAllUsers();

//        ApiClient.sendUserPostRequest( "{\n" +
//                "  \"username\": \"gkdfa54ddaadasd\",\n" +
//                "  \"name\": \"POCHEMUNERABOTAET3\",\n" +
//                "  \"first_name\": \"PROSTO\",\n" +
//                "  \"last_name\": \"OneTwoThree\",\n" +
//                "  \"email\": \"user422121211d3231@mail.com\",\n" +
//                "  \"url\": \"opyat'neponyal.com\",\n" +
//                "  \"description\": \"Opysanie2\",\n" +
//                "  \"roles\": [\n" +
//                "    \"subscriber\", \"contributor\", \"editor\", \"administrator\"\n" +
//                "  ],\n" +
//                "  \"password\": \"password\",\n" +
//                "  \"slug\": \"gjgjgj1flfk\"\n" +
//                "}");
//        System.out.println(ApiClient.responseBodyString);
//        ApiClient.sendGet("/users");
//        System.out.println(ApiClient.responseBodyString);
//        System.out.println(ApiClient.responseBodyString.contains("POCHEMUNERABOTAET3"));
//        User user = new User();
//        user.setName("POCHEMUNERABOTAET3");
//        Assert.assertTrue(UserApiHelper.findUserInResponse("name", user));



    }



}
