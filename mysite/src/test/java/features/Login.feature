Feature: Website Login

Scenario: Home page default login
Given User is on login page
When User logs in with username and password
Then User lands on dashboard page
And User see a welcome message
