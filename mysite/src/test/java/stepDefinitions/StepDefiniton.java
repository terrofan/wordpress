package stepDefinitions;

import com.telran.gui.DashboardPage;
import com.telran.gui.LoginPage;
import com.telran.tests.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;


public class StepDefiniton extends TestBase {

    @Given("^User is on login page$")
    public void user_is_on_login_page() {
        init();
        LoginPage loginPage = new LoginPage();
        loginPage.getLoginPage();
    }


    @When("^User logs in with username and password$")
    public void userLogsInWithUsernameAndPassword() {
        LoginPage loginPage = new LoginPage();
        wd.findElement(loginPage.getUsername()).sendKeys("user");
        wd.findElement(loginPage.getPassword()).sendKeys("password");
        wd.findElement(loginPage.getLoginButton()).click();

    }

    @Then("^User lands on dashboard page$")
    public void userLandsOnDashboardPage() {
        DashboardPage dash = new DashboardPage(wd);
        String expectedURL = dash.getDashboardURL();
        String actualURL = wd.getCurrentUrl();
        Assert.assertEquals(expectedURL, actualURL);
    }

    @Then("^User see a welcome message$")
    public void userSeeAWelcomeMessage() {
        System.out.println("Checking if welcome message is displayed");
    }


}
