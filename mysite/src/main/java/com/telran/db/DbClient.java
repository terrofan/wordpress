package com.telran.db;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DbClient {
    public static String SQL_SELECT;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;
    


    public static ResultSet fetchResults(){
        try {
            DBConnection.connection = DBConnection.getConnection();
            preparedStatement = DBConnection.connection.prepareStatement(SQL_SELECT);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public static ArrayList<LinkedHashMap<String, Object>> getResultsAsMap(ResultSet rs){
        ArrayList<LinkedHashMap<String, Object>> list = new ArrayList<>();
        try {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            while (rs.next()){
                LinkedHashMap<String, Object> row = new LinkedHashMap<>(columns);
                for(int i = 1; i <= columns; ++i){
                    row.put(md.getColumnLabel(i),rs.getObject(i)); // better to use 'getColumnLabel()' method
                }
                list.add(row);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return list;
    }


    public static String getResultsAsString() {
        StringBuilder sb = new StringBuilder();
        ResultSet rs = fetchResults();
        ArrayList<LinkedHashMap<String, Object>> results = getResultsAsMap(rs);
        for(LinkedHashMap<String, Object> currRow : results){
            //doesn't properly parse TIMESTAMP values,
            //I added another logic to parse such values
            for(Map.Entry<String, Object> c : currRow.entrySet()){
                if(c.getValue() instanceof Timestamp){
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss.S");
                    df.setTimeZone(TimeZone.getTimeZone("GMT"));
                    Timestamp regDate = (Timestamp) c.getValue();
                    String regDateStr = df.format(regDate);
                    sb.append(c.getKey()).append(" : ").append(regDateStr).append(", ");
                    continue;
                }
                sb.append(c.getKey()).append(" : ").append(c.getValue()).append(", ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }



}


