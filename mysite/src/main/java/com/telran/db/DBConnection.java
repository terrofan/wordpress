package com.telran.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static final String USERNAME = "root";
    private static final String PASSWORD = "password";
    private static final String CONN = "jdbc:mysql://192.168.247.5:3306/wordpress";
    public static Connection connection;

    public static Connection getConnection(){
        try {
            connection = DriverManager.getConnection(CONN, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }



}
