package com.telran.common;

import com.github.javafaker.Faker;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.hubspot.jinjava.Jinjava;
import com.telran.app.user.User;
import com.telran.report.TestReport;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RandomDataGenerator {
    public static void main(String[] args) {
        User fakeUser = generateFakeUser();
        generateNewUserRequestFromTemplateFile("user-context.json.j2", fakeUser);
    }
    public static String generateNewUserRequestFromTemplateFile(String pathToDataFile, User fakeUser) {
        Jinjava jinjava = new Jinjava();
        Map<String, Object> context = new HashMap<>();
        context.put("username", fakeUser.getUsername());
        context.put("name", fakeUser.getName());
        context.put("first_name", fakeUser.getFirstName());
        context.put("last_name", fakeUser.getLastName());
        context.put("email", fakeUser.getEmail());
        context.put("url", fakeUser.getUrl());
        context.put("description", fakeUser.getDescription());
        context.put("locale", fakeUser.getLocale());
        context.put("nickname", fakeUser.getNickname());
        context.put("slug", fakeUser.getSlug());
        context.put("roles", fakeUser.getRolesForApi());
        context.put("password", fakeUser.getPassword());

        String template = null;
        try {
            template = Resources.toString(Resources.getResource(pathToDataFile), Charsets.UTF_8);
        } catch (IOException e) {
            e.getMessage();
        }
        String render = jinjava.render(template, context);

        return render;
    }

    public static User generateFakeUser(){
        User user = new User();
        Faker faker = new Faker();
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String name = faker.name().fullName();
        String email = faker.internet().emailAddress();
        String url = faker.internet().url();
        String description = faker.chuckNorris().fact();
        String locale = "en_US";
        String nickname = faker.funnyName().name();
        String slug = faker.name().nameWithMiddle();

        user.setUsername(String.format("%s.%s", firstName.toLowerCase(), lastName.toLowerCase()));
        user.setName(name);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setUrl(url);
        user.setDescription(description);
        user.setLocale(locale);
        user.setNickname(nickname);
        user.setSlug(slug);
        user.setRoles("subscriber", "author");
        user.setPassword("password");
        return user;
    }


}
