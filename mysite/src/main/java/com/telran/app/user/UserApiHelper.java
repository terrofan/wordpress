package com.telran.app.user;

import com.telran.api.ApiClient;
import org.json.JSONArray;
import org.json.JSONObject;


public class UserApiHelper extends ApiClient {
    public static boolean findUserInResponse(String username, User user){
        ApiClient.sendUserGetRequest();
        JSONArray jsonOArray = new JSONArray(responseBodyString);
        for(int i = 0; i < jsonOArray.length(); i++){
            JSONObject jsonObject = jsonOArray.getJSONObject(i);
            if(jsonObject.get(username).equals(user.getName())){
                return true;
            }
        }
        return false;
    }
}
