package com.telran.app.user;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class User {

    private String username;
    private String name;
    private String  firstName;
    private String  lastName;
    private String email;
    private String url;
    private String description;
    private String locale;
    private String nickname;
    private String slug;
    private List<String> roles;
    private String  password; //password in DB is encrypted, how we'll assert it in test case later?

    //private Boolean sendUserNotification; -- There is no way to find out was this marker set or not (from DB tables)

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(String... roles) {
        this.roles = Arrays.asList(roles);
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String printRoles(){
        StringBuilder sb = new StringBuilder();
        for(String role : roles){
            sb.append(role).append(", ");
        }
        return sb.toString().substring(0, sb.length() - 2);
    }

    public String getRolesForApi(){
        StringBuilder sb = new StringBuilder();
        for(String role : roles){
            sb.append(role).append("\", \"");
        }
        return sb.toString().substring(0, sb.length() - 4);
    }

//    public Boolean getSendUserNotification() {
//        return sendUserNotification;
//    }
//
//    public void setSendUserNotification(Boolean sendUserNotification) {
//        this.sendUserNotification = sendUserNotification;
//    }

    public JSONObject newUserRequest() {
        JSONObject userObject = new JSONObject();

        userObject.put("username", username);
        userObject.put("name", name);
        userObject.put("first_name", firstName);
        userObject.put("last_name", lastName);
        userObject.put("email", email);
        userObject.put("url", url);
        userObject.put("description", description);
        userObject.put("locale", locale);
        userObject.put("nickname", nickname);
        userObject.put("slug", slug);
        userObject.put("roles", roles);
        userObject.put("password", password);

        return userObject;
    }

    public String newUserRequestAsString() {
        return newUserRequest().toString();
    }

    @Override
    public String toString() {
        return String.format("Username: %s | First Name: %s | Last Name: %s | Email: %s | Roles: %s", username, firstName, lastName, email, printRoles());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username) &&
                email.equals(user.email) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                roles.equals(user.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email, firstName, lastName, roles);
    }
}
