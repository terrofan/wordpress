package com.telran.app.user;

import com.telran.db.DbClient;
import com.telran.report.TestReport;

import java.sql.ResultSet;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDBHelper {

    public static User getUserByAttribute(String attribute, String value) throws Exception {

        String query = "SELECT * FROM" +
                        "(" +
                            SQLQueries.GET_ALL_USERS +
                        ") AS allUsers " +
                        "WHERE " + attribute + " = " + "\"" + value + "\"" + ";";

        return getUserByQuery(query);
    }

    public static User getUserByQuery(String query) throws Exception { //the query must contain SQLQueries.GET_ALL_USERS subquery at FROM clause.
        if(!query.contains(SQLQueries.GET_ALL_USERS)){
            throw new Exception("the query must contain SQLQueries.GET_ALL_USERS subquery at FROM clause");
        }
        DbClient.SQL_SELECT = query;
        ResultSet rs = DbClient.fetchResults();
        ArrayList<LinkedHashMap<String, Object>> userMap = DbClient.getResultsAsMap(rs);
        if(userMap.size() > 1){
            throw new Exception("More than 1 user was found by given query");
        }

        return setUserFields(userMap.get(0));
    }

    public static List<User> getAllUsers(){
        List<User> usersList = new ArrayList<>();
        DbClient.SQL_SELECT = SQLQueries.GET_ALL_USERS;
        ResultSet rs = DbClient.fetchResults();
        ArrayList<LinkedHashMap<String, Object>> users = DbClient.getResultsAsMap(rs);
        for(LinkedHashMap<String, Object> currMap : users){
            User currUser = setUserFields(currMap);
            usersList.add(currUser);
            System.out.println(currUser.getUsername());
        }

        return usersList;
    }

    private static List<String> parseRolesFromString(String roles){
        ArrayList<String> rolesArr = new ArrayList<>();

        String stringPattern = "[\"]\\w*[\"]";
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(roles);
        if(!matcher.find()){
            rolesArr.add(""); //empty string, if no roles was found
            return rolesArr;
        }
        do{
            String role = matcher.group().replaceAll("\"", "");
            rolesArr.add(role);
        }while (matcher.find());

        return rolesArr;
    }

    private static User setUserFields(Map<String, Object> userData){
        User user = new User();

        user.setUsername((String)userData.get("username"));
        user.setName((String)userData.get("name"));
        user.setFirstName((String) userData.get("firstName"));
        user.setLastName((String) userData.get("lastName"));
        user.setEmail((String) userData.get("email"));
        user.setUrl((String) userData.get("url"));
        user.setDescription((String) userData.get("description"));
        user.setLocale((String) userData.get("locale"));
        user.setRoles(parseRolesFromString((String)userData.get("roles")));
        user.setPassword((String) userData.get("password"));

        return user;
    }

    public static User createUserObjectByUsername(String username) {
        User user = new User();

        try {
            user = getUserByAttribute("username", username);
        } catch (Exception e) {
            e.getMessage();
        }
        TestReport.info("User data from db : " + user.toString());

        return user;
    }
}
