package com.telran.utils;

import java.io.*;

public class FileReader {

    public static void main(String[] args) {
        String s = readFile("src/main/resources/sample.xml");
        System.out.println(s);
        readFileAndPrint("src/main/resources/sample.xml");
    }

    public static String readFile(String path){
        StringBuilder sb = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new java.io.FileReader(path))){
            while (reader.ready()){
                sb.append(reader.readLine()).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public static void readFileAndPrint(String path){
        try(BufferedReader reader = new BufferedReader(new java.io.FileReader(path))){
            while (reader.ready()){
                System.out.println(reader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
