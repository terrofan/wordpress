package com.telran.gui;

import com.telran.tests.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class LoginPage extends Page {

    private String URL = "http://192.168.247.5:8080/wp-login.php";
    private By wpLogo = By.xpath("//h1/a");
    private By username = By.id("user_login");
    private By password = By.id("user_pass");
    private By rememberMe = By.id("rememberme");
    private By loginButton = By.id("wp-submit");
    private By lostYourPassword = By.linkText("Lost your password?");
    private By backToMySite = By.linkText("← Back to My Site");

    public String getCurrentURL() {
        return URL;
    }

    public void getLoginPage(){
        wd.get(URL);
    }

    public By getWpLogo() {
        return wpLogo;
    }

    public By getUsername() {
        return username;
    }

    public By getPassword() {
        return password;
    }

    public By getRememberMe() {
        return rememberMe;
    }

    public By getLoginButton() {
        return loginButton;
    }

    public By getLostYourPassword() {
        return lostYourPassword;
    }

    public By getBackToMySite() {
        return backToMySite;
    }
}
