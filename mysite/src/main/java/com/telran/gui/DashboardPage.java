package com.telran.gui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage extends Page{
    private WebDriver wd;

    public DashboardPage(WebDriver wd) {
        this.wd = wd;
    }

    public String getDashboardURL(){
        return "http://192.168.247.5:8080/wp-admin/";
    }


}
