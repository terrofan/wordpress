package com.telran.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class TestBase {
    public static WebDriver wd;
    public static Properties prop;

    public static void setProperties(){
        prop = new Properties();
        try {
            FileInputStream fis = new FileInputStream("src/main/resources/prop.properties");
            prop.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @BeforeTest
    public void init() {
        setProperties();
        if(prop.getProperty("browser").equals("chrome")){
//            Map<String, Object> prefs = new HashMap<String, Object>();
//            prefs.put("profile.default_content_setting_values.notifications", 2);
//            ChromeOptions options = new ChromeOptions();
//            options.setExperimentalOption("prefs", prefs);
//            String chromeDriverPath = prop.getProperty("chromeDriverPath");
//            System.setProperty("webdriver.chrome.driver", chromeDriverPath);
//            wd = new ChromeDriver(options);
            System.setProperty("webdriver.chrome.driver", "C:/Tools/chromedriver.exe");
            wd = new ChromeDriver();
        }if(prop.getProperty("browser").equals("firefox")){
            //TODO
        }if(prop.getProperty("browser").equals("IE")){
            //TODO
        }if(prop.getProperty("browser").equals("safari")){
            //TODO
        }

    }

    @AfterTest
    public void tearDown(){
        wd.quit();
    }
}
