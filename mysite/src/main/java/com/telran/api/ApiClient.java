package com.telran.api;
import okhttp3.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.IOException;

public class ApiClient {

    private static final String BASE_URL = "http://192.168.247.5:8080/wp-json/wp/v2";
    private static final OkHttpClient httpClient = new OkHttpClient();
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final Logger logger = LoggerFactory.getLogger(ApiClient.class);
    protected static Response response;
    public static String responseBodyString;

    public static void sendGet(String endPoint) throws IOException, InterruptedException {
        logger.debug("Preparing request to RESTApi {}{}", BASE_URL, endPoint);
        Request request = new Request.Builder()
                .url(BASE_URL + endPoint + "?per_page=100")
                .addHeader("Authorization", Credentials.basic("user", "password"))
                .build();
        logger.debug("Executing request");
        Call call = httpClient.newCall(request);
//        Thread.sleep(5000);
        response = call.execute();
        logger.debug("Response code from resource : {}", response.code());
        responseBodyString = response.body().string();

        logger.info("Response body string : {}", responseBodyString);
//        Headers responseHeaders = response.headers();
//        for(int i = 0; i < responseHeaders.size(); i++){
//            logger.info("Header {} : {}", responseHeaders.name(i), responseHeaders.value(i));
//        }
    }

    public static void sendPost(String endPoint, String jsonBody) throws IOException{

        RequestBody body = RequestBody.create(JSON, jsonBody); // changed the order of parameters, since we've downgraded okhttp client to 3.11.0

        Request request = new Request.Builder()
                .url(BASE_URL + endPoint)
                .addHeader("User-Agent", "OkHttp Bot")
                .addHeader("Authorization", Credentials.basic("user", "password"))
                .post(body)
                .build();
        response = httpClient.newCall(request).execute();
        responseBodyString = response.body().string();
        //        if(!response.isSuccessful()) throw new IOException("Unexpected code " + response);


    }


    public static Response sendUserPostRequest(String requestAsString) {
        logger.debug("Sending POST request to {}{}", BASE_URL, ApiEndPoints.users);
        try {
            sendPost(ApiEndPoints.users, requestAsString);
        } catch (IOException e) {
            logger.error("Failed to send POST request to {}{} API", BASE_URL, ApiEndPoints.users);
            e.getMessage();
        }
        return response;
    }

    public static Response sendUserGetRequest() {
        logger.debug("Sending GET request to {}{}", BASE_URL, ApiEndPoints.users);
        try {
            sendGet(ApiEndPoints.users);
        } catch (IOException | InterruptedException e) {
            logger.error("Failed to send GET request to {}{} API", BASE_URL, ApiEndPoints.users);
            e.getMessage();
        }
        return response;
    }

    public static boolean verifyResponseCode(int expectedResponseCode) {
        return expectedResponseCode == response.code();

    }

    public static boolean verifyFieldInResponse(String responseKey, String expectedValue) {
        // TODO

        JSONObject responseBodyAsJSON = new JSONObject(responseBodyString);
        return expectedValue.equals(responseBodyAsJSON.get(responseKey));
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        //sendPost("{\"username\":\"user12\",\"first_name\":\"\",\"last_name\":\"\",\"email\":\"user12@mail.com\",\"url\":\"\",\"description\":\"\",\"roles\":[\"subscriber\"],\"password\":\"password\"}");
        sendGet("/users");
        verifyFieldInResponse("username", "user");
    }
}
